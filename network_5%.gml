Creator "igraph version 1.2.6 Tue Jul  6 11:29:05 2021"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "Cluster_38"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_38"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 25.7323982142857
    size2 6.85433571428571
  ]
  node
  [
    id 1
    name "Cluster_10"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_10"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 25.7323982142857
    size2 6.85433571428571
  ]
  node
  [
    id 2
    name "Cluster_152"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_152"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 3
    name "Cluster_40"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_40"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 25.7323982142857
    size2 6.85433571428571
  ]
  node
  [
    id 4
    name "Cluster_1227"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_1227"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 31.0402125
    size2 6.85433571428571
  ]
  node
  [
    id 5
    name "Cluster_8"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_8"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 23.0784910714286
    size2 6.85433571428571
  ]
  node
  [
    id 6
    name "Cluster_417"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_417"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 7
    name "Cluster_189"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_189"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 8
    name "Cluster_300"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_300"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 9
    name "Cluster_376"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_376"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 10
    name "Cluster_37"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_37"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 25.7323982142857
    size2 6.85433571428571
  ]
  node
  [
    id 11
    name "Cluster_274"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_274"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 12
    name "Cluster_214"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_214"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 13
    name "Cluster_577"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_577"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 14
    name "Cluster_370"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_370"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 15
    name "Cluster_22"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_22"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 25.7323982142857
    size2 6.85433571428571
  ]
  node
  [
    id 16
    name "Cluster_905"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_905"
    color "darkorchid"
    shape "circle"
    labelcex 0.610598341689931
    size 28.3863053571429
    size2 6.85433571428571
  ]
  node
  [
    id 17
    name "M98T1981"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M98T1981"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 25.4698714285714
    size2 6.85433571428571
  ]
  node
  [
    id 18
    name "M179T2426"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M179T2426"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 19
    name "M192T2217"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M192T2217"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 20
    name "M193T1031"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M193T1031"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 21
    name "M237T897"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M237T897"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 25.4698714285714
    size2 6.85433571428571
  ]
  node
  [
    id 22
    name "M242T1483"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M242T1483"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 23
    name "M254T2680"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M254T2680"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 24
    name "M264T1334"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M264T1334"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 25
    name "M269T2675"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M269T2675"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 26
    name "M273T1296"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M273T1296"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 27
    name "M284T984"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M284T984"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 25.4698714285714
    size2 6.85433571428571
  ]
  node
  [
    id 28
    name "M290T1763"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M290T1763"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 29
    name "M317T1237"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M317T1237"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 30
    name "M317T2443"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M317T2443"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 31
    name "M332T1385"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M332T1385"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 32
    name "M354T1895"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M354T1895"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 33
    name "M360T2013"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M360T2013"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 34
    name "M365T1296"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M365T1296"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 35
    name "M373T1907"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M373T1907"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 36
    name "M374T1907"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M374T1907"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 37
    name "M377T2083"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M377T2083"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 38
    name "M379T1636"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M379T1636"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 39
    name "M379T1873"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M379T1873"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 40
    name "M381T2431"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M381T2431"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 41
    name "M387T1950"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M387T1950"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 42
    name "M446T2183"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M446T2183"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 43
    name "M446T2159"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M446T2159"
    color "brown1"
    shape "circle"
    labelcex 0.610598341689931
    size 28.1237785714286
    size2 6.85433571428571
  ]
  node
  [
    id 44
    name "1-(1-enyl-stearoyl)-2-oleoyl-GPE (P-18:0/18:1)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "1-(1-enyl-stearoyl)-2-oleoyl-GPE (P-18:0/18:1)"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 108.5572125
    size2 6.85433571428571
  ]
  node
  [
    id 45
    name "1-methylhistidine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "1-methylhistidine"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 39.5174410714286
    size2 6.85433571428571
  ]
  node
  [
    id 46
    name "3-indoxyl sulfate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "3-indoxyl sulfate"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 38.0520642857143
    size2 6.85433571428571
  ]
  node
  [
    id 47
    name "3-methylhistidine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "3-methylhistidine"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 39.5174410714286
    size2 6.85433571428571
  ]
  node
  [
    id 48
    name "4-guanidinobutanoate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "4-guanidinobutanoate"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 49.9278214285714
    size2 6.85433571428571
  ]
  node
  [
    id 49
    name "5-methyl-2'-deoxycytidine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "5-methyl-2'-deoxycytidine"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 60.0709017857143
    size2 6.85433571428571
  ]
  node
  [
    id 50
    name "chiro-inositol"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "chiro-inositol"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 30.6392625
    size2 6.85433571428571
  ]
  node
  [
    id 51
    name "dimethylarginine (SDMA + ADMA)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "dimethylarginine (SDMA + ADMA)"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 74.7867214285714
    size2 6.85433571428571
  ]
  node
  [
    id 52
    name "gamma-tocopherol/beta-tocopherol"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "gamma-tocopherol/beta-tocopherol"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 79.3308214285714
    size2 6.85433571428571
  ]
  node
  [
    id 53
    name "methylmalonate (MMA)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "methylmalonate (MMA)"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 51.8418803571429
    size2 6.85433571428571
  ]
  node
  [
    id 54
    name "N-delta-acetylornithine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "N-delta-acetylornithine"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 52.9158535714286
    size2 6.85433571428571
  ]
  node
  [
    id 55
    name "ophthalmate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "ophthalmate"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 28.9209053571429
    size2 6.85433571428571
  ]
  node
  [
    id 56
    name "orotidine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "orotidine"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 20.9591839285714
    size2 6.85433571428571
  ]
  node
  [
    id 57
    name "phenol sulfate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "phenol sulfate"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 32.2287428571429
    size2 6.85433571428571
  ]
  node
  [
    id 58
    name "stachydrine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "stachydrine"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 26.9877535714286
    size2 6.85433571428571
  ]
  node
  [
    id 59
    name "trigonelline (N'-methylnicotinate)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "trigonelline (N'-methylnicotinate)"
    color "lightgreen"
    shape "circle"
    labelcex 0.610598341689931
    size 71.9466589285714
    size2 6.85433571428571
  ]
  edge
  [
    source 18
    target 0
    weight 0.834509599487997
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 20
    target 0
    weight 0.840555487138676
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 21
    target 0
    weight -0.837879760497366
    labelcolor "black"
    color "#007D00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 22
    target 0
    weight 0.861090502439712
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 24
    target 0
    weight 0.823784173415273
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 27
    target 0
    weight 0.814582082696069
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 28
    target 0
    weight 0.877492358051173
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 29
    target 0
    weight 0.803732322076162
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 30
    target 0
    weight 0.832442037742054
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 0
    weight -0.892780364880031
    labelcolor "black"
    color "#00B300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 33
    target 0
    weight 0.809491836762954
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 37
    target 0
    weight -0.826356185662433
    labelcolor "black"
    color "#007200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 0
    weight -0.86461398387775
    labelcolor "black"
    color "#009700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 0
    weight -0.865346111958917
    labelcolor "black"
    color "#009700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 40
    target 0
    weight 0.803704125735371
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 17
    target 1
    weight -0.77181719178682
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 35
    target 1
    weight -0.765187511332721
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 36
    target 1
    weight -0.762551801809558
    labelcolor "black"
    color "#004200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 41
    target 1
    weight -0.778090708402772
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 42
    target 1
    weight -0.725415652366689
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 43
    target 1
    weight -0.725220699782746
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 18
    target 2
    weight 0.761119572711587
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 20
    target 2
    weight 0.727692875878591
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 21
    target 2
    weight -0.722700665720148
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 22
    target 2
    weight 0.768629067799317
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 24
    target 2
    weight 0.749016105006862
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 27
    target 2
    weight 0.744854288397054
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 28
    target 2
    weight 0.762722160205796
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 29
    target 2
    weight 0.706782324199522
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 30
    target 2
    weight 0.761339383210747
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 2
    weight -0.77513019617896
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 33
    target 2
    weight 0.748029134937561
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 37
    target 2
    weight -0.723901178070059
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 2
    weight -0.767557672368843
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 2
    weight -0.762164472285738
    labelcolor "black"
    color "#004200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 40
    target 2
    weight 0.75985017138188
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 18
    target 3
    weight 0.845635476378534
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 19
    target 3
    weight 0.706002172231589
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 20
    target 3
    weight 0.85748030651313
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 21
    target 3
    weight -0.846616687713844
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 22
    target 3
    weight 0.867380999266089
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 24
    target 3
    weight 0.837041170765288
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 27
    target 3
    weight 0.83258921319384
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 28
    target 3
    weight 0.876295437291465
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 29
    target 3
    weight 0.81970184356134
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 30
    target 3
    weight 0.844760261616569
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 3
    weight -0.906251772201929
    labelcolor "black"
    color "#00C300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 33
    target 3
    weight 0.834510006491934
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 37
    target 3
    weight -0.84262757444168
    labelcolor "black"
    color "#008300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 3
    weight -0.875684559736425
    labelcolor "black"
    color "#00A300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 3
    weight -0.876571780790313
    labelcolor "black"
    color "#00A300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 40
    target 3
    weight 0.823973289846963
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 20
    target 4
    weight 0.705491821231019
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 21
    target 4
    weight -0.704374597980799
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 22
    target 4
    weight 0.719925914447073
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 28
    target 4
    weight 0.729110623831995
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 4
    weight -0.747071971452002
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 4
    weight -0.734110479535442
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 4
    weight -0.73367486511488
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 17
    target 5
    weight -0.757217253158314
    labelcolor "black"
    color "#004000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 35
    target 5
    weight -0.766508307148195
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 36
    target 5
    weight -0.746119095014943
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 41
    target 5
    weight -0.74697172528048
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 42
    target 5
    weight -0.727688949693931
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 43
    target 5
    weight -0.726562881511539
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 17
    target 6
    weight -0.777876826760947
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 31
    target 6
    weight -0.721763291730184
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 35
    target 6
    weight -0.772979968490546
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 36
    target 6
    weight -0.787805015772439
    labelcolor "black"
    color "#005400FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 41
    target 6
    weight -0.763050712272983
    labelcolor "black"
    color "#004200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 42
    target 6
    weight -0.749856367358674
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 43
    target 6
    weight -0.757185786750725
    labelcolor "black"
    color "#004000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 18
    target 7
    weight 0.847428021449852
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 20
    target 7
    weight 0.870663785438696
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 21
    target 7
    weight -0.862653035654678
    labelcolor "black"
    color "#009700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 22
    target 7
    weight 0.876081002106181
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 24
    target 7
    weight 0.840289109013818
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 27
    target 7
    weight 0.832790191712255
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 28
    target 7
    weight 0.891980337556166
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 29
    target 7
    weight 0.827716401035964
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 30
    target 7
    weight 0.846457837309171
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 7
    weight -0.9206405311847
    labelcolor "black"
    color "#00CE00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 33
    target 7
    weight 0.832256766983819
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 37
    target 7
    weight -0.853753462482674
    labelcolor "black"
    color "#008D00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 7
    weight -0.886189419724449
    labelcolor "black"
    color "#00AD00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 7
    weight -0.888717223900382
    labelcolor "black"
    color "#00AD00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 40
    target 7
    weight 0.817458406446675
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 18
    target 8
    weight 0.763433408271937
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 20
    target 8
    weight 0.771516798855051
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 21
    target 8
    weight -0.769205630703933
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 22
    target 8
    weight 0.788850585434822
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 24
    target 8
    weight 0.75931968478738
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 27
    target 8
    weight 0.749399589368142
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 28
    target 8
    weight 0.796117148377776
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 29
    target 8
    weight 0.733805413114788
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 30
    target 8
    weight 0.765764500843846
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 8
    weight -0.818810859447586
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 33
    target 8
    weight 0.753438768620521
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 37
    target 8
    weight -0.761751757838567
    labelcolor "black"
    color "#004200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 8
    weight -0.801079203646279
    labelcolor "black"
    color "#005B00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 8
    weight -0.800486028359765
    labelcolor "black"
    color "#005B00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 40
    target 8
    weight 0.747941112813835
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 18
    target 9
    weight 0.782763079882991
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 20
    target 9
    weight 0.793593757357774
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 21
    target 9
    weight -0.786954689911174
    labelcolor "black"
    color "#005400FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 22
    target 9
    weight 0.803461910136617
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 24
    target 9
    weight 0.768210889986087
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 27
    target 9
    weight 0.769024142888321
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 28
    target 9
    weight 0.814097324418864
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 29
    target 9
    weight 0.773818899292189
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 30
    target 9
    weight 0.780961575608701
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 9
    weight -0.836343037419216
    labelcolor "black"
    color "#007D00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 33
    target 9
    weight 0.767734204780599
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 37
    target 9
    weight -0.776144520086075
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 9
    weight -0.818743940559533
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 9
    weight -0.818865087764273
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 40
    target 9
    weight 0.758024862066342
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 18
    target 10
    weight 0.866898542320183
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 20
    target 10
    weight 0.854013640764465
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 21
    target 10
    weight -0.848933405145327
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 22
    target 10
    weight 0.880613338461732
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 24
    target 10
    weight 0.842702778283597
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 27
    target 10
    weight 0.84986690862536
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 28
    target 10
    weight 0.883966633389829
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 29
    target 10
    weight 0.853041128313039
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 30
    target 10
    weight 0.864503614028569
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 10
    weight -0.899872676297543
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 33
    target 10
    weight 0.848304540076676
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 37
    target 10
    weight -0.836716804360706
    labelcolor "black"
    color "#007D00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 10
    weight -0.899086244653834
    labelcolor "black"
    color "#00B800FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 10
    weight -0.895160781823824
    labelcolor "black"
    color "#00B800FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 40
    target 10
    weight 0.848584239996268
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 11
    weight -0.707410533812234
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 11
    weight -0.71821954052003
    labelcolor "black"
    color "#002700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 11
    weight -0.712226279322741
    labelcolor "black"
    color "#002300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 17
    target 12
    weight -0.750013705934944
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 35
    target 12
    weight -0.727511340232184
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 36
    target 12
    weight -0.768946804964975
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 41
    target 12
    weight -0.744586004125599
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 42
    target 12
    weight -0.719469882392017
    labelcolor "black"
    color "#002700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 43
    target 12
    weight -0.731160555112101
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 36
    target 13
    weight -0.708559763119529
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 17
    target 14
    weight -0.77777303309099
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 35
    target 14
    weight -0.773198039356382
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 36
    target 14
    weight -0.769694404354566
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 41
    target 14
    weight -0.774385224032658
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 42
    target 14
    weight -0.733661075944924
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 43
    target 14
    weight -0.734487586240594
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 15
    weight -0.703592783025183
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 33
    target 15
    weight 0.703477663599993
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 38
    target 15
    weight -0.711929505798329
    labelcolor "black"
    color "#002300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 39
    target 15
    weight -0.705624685786971
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 40
    target 15
    weight 0.704669879436821
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 32
    target 16
    weight -0.700792057135364
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 0
    weight 0.751373435180143
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 0
    weight 0.731876444667249
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 0
    weight 0.725688516728156
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 0
    weight -0.701361727684656
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 0
    weight 0.833580527814989
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 0
    weight 0.713592733438729
    labelcolor "black"
    color "#320000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 0
    weight 0.785856318492572
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 0
    weight 0.892839540338146
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 2
    weight 0.767004419232052
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 2
    weight 0.792237561454494
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 3
    weight 0.781374503681202
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 3
    weight 0.745886422493762
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 3
    weight 0.75231253504619
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 3
    weight -0.71402486265566
    labelcolor "black"
    color "#002300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 3
    weight 0.858715725375548
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 3
    weight 0.70601920598433
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 3
    weight 0.774804412984902
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 3
    weight 0.91000771261268
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 4
    weight 0.707865435909529
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 4
    weight 0.74873281977757
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 7
    weight 0.784038458743489
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 7
    weight 0.743955543506252
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 7
    weight 0.756623898395624
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 7
    weight -0.711032580980729
    labelcolor "black"
    color "#002300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 7
    weight 0.85807358281028
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 7
    weight 0.715712260344967
    labelcolor "black"
    color "#320000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 7
    weight 0.808183591170924
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 7
    weight 0.918308272697236
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 8
    weight 0.778547127399308
    labelcolor "black"
    color "#6C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 8
    weight 0.703908752660179
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 8
    weight 0.82034191734824
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 9
    weight 0.723386364266274
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 9
    weight 0.793095226392803
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 9
    weight 0.719402889935856
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 9
    weight 0.841373327497503
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 46
    target 10
    weight 0.702797888245665
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 10
    weight 0.786223966231488
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 10
    weight 0.747922115781335
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 10
    weight 0.732316062258623
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 10
    weight -0.764264989994021
    labelcolor "black"
    color "#004200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 10
    weight 0.876519161093651
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 10
    weight 0.732433861964722
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 10
    weight 0.74770463795819
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 10
    weight 0.91635204719955
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 11
    weight 0.716983399690784
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 11
    weight 0.723349179916938
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 15
    weight 0.73024646375453
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 15
    weight 0.718152769909209
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 16
    weight 0.711397227041063
    labelcolor "black"
    color "#320000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 45
    target 17
    weight 0.751456864207442
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 47
    target 17
    weight 0.735061110575088
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 18
    weight 0.794562835465538
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 18
    weight 0.826220590446003
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 18
    weight 0.750971552644057
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 18
    weight -0.766735537020268
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 18
    weight 0.887547894856377
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 18
    weight 0.74881604274451
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 18
    weight 0.70304032385666
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 18
    weight 0.924036522476495
    labelcolor "black"
    color "#DE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 19
    weight 0.760964468511907
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 19
    weight 0.781023290210473
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 19
    weight 0.770281326960092
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 20
    weight 0.801522521118816
    labelcolor "black"
    color "#7F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 20
    weight 0.724095047646106
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 20
    weight 0.758311161006766
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 20
    weight -0.715653576809908
    labelcolor "black"
    color "#002300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 20
    weight 0.862399907506185
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 20
    weight 0.800245400363201
    labelcolor "black"
    color "#7F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 20
    weight 0.918423205895273
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 21
    weight -0.765545926534038
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 21
    weight -0.730320702566441
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 21
    weight 0.71799776720907
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 21
    weight -0.847988228356204
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 21
    weight -0.710243947595957
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 21
    weight -0.810520633249586
    labelcolor "black"
    color "#006300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 21
    weight -0.904299767619806
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 46
    target 22
    weight 0.717858258252371
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 22
    weight 0.786576475137581
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 22
    weight 0.798079950971562
    labelcolor "black"
    color "#7F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 22
    weight 0.757184192110857
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 22
    weight -0.768227437258796
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 22
    weight 0.893363158992572
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 22
    weight 0.762192544231142
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 22
    weight 0.768021886104342
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 22
    weight 0.937872635785754
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 44
    target 23
    weight 0.721221874577621
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 53
    target 23
    weight 0.778328704486524
    labelcolor "black"
    color "#6C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 24
    weight 0.763200803332369
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 24
    weight 0.802726002436475
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 24
    weight 0.754311334245174
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 24
    weight -0.73968795846619
    labelcolor "black"
    color "#003600FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 24
    weight 0.878902449870517
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 24
    weight 0.723956303454314
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 24
    weight 0.700394465003673
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 24
    weight 0.908037639265995
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 53
    target 25
    weight 0.709174519671153
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 56
    target 26
    weight -0.751812959473276
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 27
    weight 0.791062343981783
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 27
    weight 0.802439811038082
    labelcolor "black"
    color "#7F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 27
    weight 0.749964969362027
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 27
    weight -0.749439720127255
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 27
    weight 0.879028708013301
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 27
    weight 0.717813730642101
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 27
    weight 0.909534256506331
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 46
    target 28
    weight 0.706042367356599
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 28
    weight 0.790691711213809
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 28
    weight 0.782952688237224
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 28
    weight 0.750151347796876
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 28
    weight -0.754079312290671
    labelcolor "black"
    color "#004000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 28
    weight 0.876008996295724
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 28
    weight 0.771353762646893
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 28
    weight 0.822156733953205
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 28
    weight 0.941079602173148
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 29
    weight 0.806648363538284
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 29
    weight 0.701689364506456
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 29
    weight -0.737283545029159
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 29
    weight 0.835590021313359
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 29
    weight 0.751309283781284
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 29
    weight 0.891734495603389
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 30
    weight 0.782128304552898
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 30
    weight 0.813830925091584
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 30
    weight 0.751367862150043
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 30
    weight -0.767716101851557
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 30
    weight 0.891381306293443
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 55
    target 30
    weight -0.705907300275867
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 30
    weight 0.744611604786189
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 30
    weight 0.701929376120579
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 30
    weight 0.921476467393623
    labelcolor "black"
    color "#DE0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 46
    target 32
    weight -0.703961883722831
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 32
    weight -0.830499532698395
    labelcolor "black"
    color "#007700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 32
    weight -0.780477225575652
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 32
    weight -0.800633786992057
    labelcolor "black"
    color "#005B00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 32
    weight 0.756863670093407
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 32
    weight -0.912190904917073
    labelcolor "black"
    color "#00C800FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 32
    weight -0.750170590988221
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 32
    weight -0.842307186590339
    labelcolor "black"
    color "#008300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 32
    weight -0.969734757368764
    labelcolor "black"
    color "#00FF00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 33
    weight 0.785240873238503
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 33
    weight 0.791670844453858
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 33
    weight 0.763162906385548
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 33
    weight -0.753358157665918
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 33
    weight 0.894804437028422
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 33
    weight 0.7037848118634
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 33
    weight 0.910890728858347
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 56
    target 34
    weight -0.73633478098833
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 45
    target 35
    weight 0.753794149753198
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 47
    target 35
    weight 0.73769354711238
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 45
    target 36
    weight 0.789979860146928
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 47
    target 36
    weight 0.750120524372609
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 49
    target 36
    weight 0.758776375475006
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 37
    weight -0.772239819251745
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 37
    weight -0.728491487434165
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 37
    weight -0.751429626427064
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 37
    weight 0.70846160058622
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 37
    weight -0.857525286109972
    labelcolor "black"
    color "#009200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 37
    weight -0.768325755893714
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 37
    weight -0.902965992892729
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 46
    target 38
    weight -0.766831511796626
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 38
    weight -0.788343605954839
    labelcolor "black"
    color "#005400FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 38
    weight -0.732552087023414
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 38
    weight -0.749764455975616
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 38
    weight 0.789436181982641
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 38
    weight -0.905039432329462
    labelcolor "black"
    color "#00C300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 55
    target 38
    weight 0.718756618620609
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 38
    weight -0.756070000069576
    labelcolor "black"
    color "#004000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 38
    weight -0.803318611730704
    labelcolor "black"
    color "#005F00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 38
    weight -0.94393218062055
    labelcolor "black"
    color "#00E900FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 46
    target 39
    weight -0.754330923969429
    labelcolor "black"
    color "#004000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 39
    weight -0.790867933942539
    labelcolor "black"
    color "#005400FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 39
    weight -0.72530148180042
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 39
    weight -0.751424509566821
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 39
    weight 0.778652627163784
    labelcolor "black"
    color "#6C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 39
    weight -0.8988975335919
    labelcolor "black"
    color "#00B800FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 55
    target 39
    weight 0.700274717378284
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 39
    weight -0.749369218102595
    labelcolor "black"
    color "#003C00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 58
    target 39
    weight -0.814940476809899
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 39
    weight -0.942524403135889
    labelcolor "black"
    color "#00E300FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 46
    target 40
    weight 0.700854329290854
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 48
    target 40
    weight 0.766026837832738
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 50
    target 40
    weight 0.827436373628163
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 51
    target 40
    weight 0.75161951261594
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 52
    target 40
    weight -0.773308267387848
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 54
    target 40
    weight 0.90053931081956
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 55
    target 40
    weight -0.730427613320435
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 57
    target 40
    weight 0.726190952481406
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 59
    target 40
    weight 0.906340555257971
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 45
    target 41
    weight 0.783499550965416
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 47
    target 41
    weight 0.746649703561897
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 45
    target 42
    weight 0.778253331629444
    labelcolor "black"
    color "#6C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 49
    target 42
    weight 0.746339693555183
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 45
    target 43
    weight 0.789593477729402
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
  edge
  [
    source 49
    target 43
    weight 0.777170884796498
    labelcolor "black"
    color "#6C0000FF"
    lty "solid"
    width 1
    labelcex 0.549538507520938
  ]
]
