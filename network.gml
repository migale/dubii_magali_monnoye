Creator "igraph version 1.2.6 Wed Jun  2 15:38:00 2021"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "Cluster_38"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_38"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 16.038
    size2 4.343625
  ]
  node
  [
    id 1
    name "Cluster_10"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_10"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 16.038
    size2 4.343625
  ]
  node
  [
    id 2
    name "Cluster_152"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_152"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 3
    name "Cluster_388"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_388"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 4
    name "Cluster_40"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_40"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 16.038
    size2 4.343625
  ]
  node
  [
    id 5
    name "Cluster_7"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_7"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 14.367375
    size2 4.343625
  ]
  node
  [
    id 6
    name "Cluster_400"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_400"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 7
    name "Cluster_103"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_103"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 8
    name "Cluster_52"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_52"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 16.038
    size2 4.343625
  ]
  node
  [
    id 9
    name "Cluster_163"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_163"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 10
    name "Cluster_417"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_417"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 11
    name "Cluster_419"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_419"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 12
    name "Cluster_734"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_734"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 13
    name "Cluster_189"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_189"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 14
    name "Cluster_300"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_300"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 15
    name "Cluster_450"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_450"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 16
    name "Cluster_376"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_376"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 17
    name "Cluster_37"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_37"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 16.038
    size2 4.343625
  ]
  node
  [
    id 18
    name "Cluster_214"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_214"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 19
    name "Cluster_253"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_253"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 20
    name "Cluster_577"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_577"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 21
    name "Cluster_156"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_156"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 22
    name "Cluster_370"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_370"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 23
    name "Cluster_22"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_22"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 16.038
    size2 4.343625
  ]
  node
  [
    id 24
    name "Cluster_117"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_117"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 25
    name "Cluster_372"
    group "microbiote"
    labelcolor "black"
    labelfamily "sans"
    label "Cluster_372"
    color "darkorchid"
    shape "circle"
    labelcex 0.535007826856959
    size 17.708625
    size2 4.343625
  ]
  node
  [
    id 26
    name "M104T618"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M104T618"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 27
    name "M111T2680"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M111T2680"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 28
    name "M117T620"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M117T620"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 29
    name "M120T422"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M120T422"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 30
    name "M125T2032"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M125T2032"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 31
    name "M128T2655"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M128T2655"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 32
    name "M179T2426"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M179T2426"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 33
    name "M193T1031"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M193T1031"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 34
    name "M205T620"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M205T620"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 35
    name "M219T731"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M219T731"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 36
    name "M222T752"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M222T752"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 37
    name "M237T897"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M237T897"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 38
    name "M242T1483"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M242T1483"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 39
    name "M245T681"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M245T681"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 40
    name "M249T1022"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M249T1022"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 41
    name "M254T2680"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M254T2680"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 42
    name "M261T2143"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M261T2143"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 43
    name "M264T1334"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M264T1334"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 44
    name "M267T1307"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M267T1307"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 45
    name "M268T1612"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M268T1612"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 46
    name "M268T1857"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M268T1857"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 47
    name "M269T2675"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M269T2675"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 48
    name "M273T1296"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M273T1296"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 49
    name "M284T984"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M284T984"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 15.703875
    size2 4.343625
  ]
  node
  [
    id 50
    name "M290T1763"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M290T1763"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 51
    name "M306T1839"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M306T1839"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 52
    name "M311T1541"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M311T1541"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 53
    name "M317T1237"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M317T1237"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 54
    name "M317T2443"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M317T2443"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 55
    name "M317T2401"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M317T2401"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 56
    name "M332T1385"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M332T1385"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 57
    name "M354T1895"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M354T1895"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 58
    name "M357T2670"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M357T2670"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 59
    name "M358T2671"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M358T2671"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 60
    name "M360T2013"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M360T2013"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 61
    name "M365T1296"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M365T1296"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 62
    name "M373T1907"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M373T1907"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 63
    name "M374T1907"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M374T1907"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 64
    name "M374T1545"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M374T1545"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 65
    name "M375T2471"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M375T2471"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 66
    name "M377T2083"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M377T2083"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 67
    name "M379T1636"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M379T1636"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 68
    name "M379T1873"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M379T1873"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 69
    name "M381T2431"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M381T2431"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 70
    name "M398T1178"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M398T1178"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 71
    name "M445T1786"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M445T1786"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 72
    name "M446T2183"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M446T2183"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 73
    name "M446T2159"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M446T2159"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 74
    name "M447T2412"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M447T2412"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 75
    name "M454T1977"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M454T1977"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 76
    name "M467T1296"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M467T1296"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 77
    name "M502T2315"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M502T2315"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 78
    name "M618T2422"
    group "caecum"
    labelcolor "black"
    labelfamily "sans"
    label "M618T2422"
    color "brown1"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 79
    name "1-(1-enyl-stearoyl)-2-oleoyl-GPE (P-18:0/18:1)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "1-(1-enyl-stearoyl)-2-oleoyl-GPE (P-18:0/18:1)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 62.3143125
    size2 4.343625
  ]
  node
  [
    id 80
    name "10-heptadecenoate (17:1n7)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "10-heptadecenoate (17:1n7)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 39.5938125
    size2 4.343625
  ]
  node
  [
    id 81
    name "1-methylhistidine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "1-methylhistidine"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 24.057
    size2 4.343625
  ]
  node
  [
    id 82
    name "1-myristoylglycerol (14:0)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "1-myristoylglycerol (14:0)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 35.083125
    size2 4.343625
  ]
  node
  [
    id 83
    name "2-hydroxyglutarate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "2-hydroxyglutarate"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 26.5629375
    size2 4.343625
  ]
  node
  [
    id 84
    name "2-palmitoylglycerol (16:0)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "2-palmitoylglycerol (16:0)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 35.083125
    size2 4.343625
  ]
  node
  [
    id 85
    name "3-hydroxyoctanoate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "3-hydroxyoctanoate"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 28.0665
    size2 4.343625
  ]
  node
  [
    id 86
    name "3-methylhistidine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "3-methylhistidine"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 24.057
    size2 4.343625
  ]
  node
  [
    id 87
    name "4-guanidinobutanoate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "4-guanidinobutanoate"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 30.7395
    size2 4.343625
  ]
  node
  [
    id 88
    name "5-methyl-2'-deoxycytidine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "5-methyl-2'-deoxycytidine"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 35.41725
    size2 4.343625
  ]
  node
  [
    id 89
    name "dihomo-linolenate (20:3n3 or n6)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "dihomo-linolenate (20:3n3 or n6)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 44.9398125
    size2 4.343625
  ]
  node
  [
    id 90
    name "docosapentaenoate (n6 DPA; 22:5n6)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "docosapentaenoate (n6 DPA; 22:5n6)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 51.45525
    size2 4.343625
  ]
  node
  [
    id 91
    name "glucuronate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "glucuronate"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 17.3745
    size2 4.343625
  ]
  node
  [
    id 92
    name "glutarate (pentanedioate)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "glutarate (pentanedioate)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 35.2501875
    size2 4.343625
  ]
  node
  [
    id 93
    name "methylmalonate (MMA)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "methylmalonate (MMA)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 31.9089375
    size2 4.343625
  ]
  node
  [
    id 94
    name "N6-carboxymethyllysine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "N6-carboxymethyllysine"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 33.2454375
    size2 4.343625
  ]
  node
  [
    id 95
    name "N-acetylglutamate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "N-acetylglutamate"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 25.5605625
    size2 4.343625
  ]
  node
  [
    id 96
    name "N-delta-acetylornithine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "N-delta-acetylornithine"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 31.5748125
    size2 4.343625
  ]
  node
  [
    id 97
    name "N-formylphenylalanine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "N-formylphenylalanine"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 31.2406875
    size2 4.343625
  ]
  node
  [
    id 98
    name "orotidine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "orotidine"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 13.1979375
    size2 4.343625
  ]
  node
  [
    id 99
    name "oxalate (ethanedioate)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "oxalate (ethanedioate)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 31.5748125
    size2 4.343625
  ]
  node
  [
    id 100
    name "palmitate (16:0)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "palmitate (16:0)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 22.386375
    size2 4.343625
  ]
  node
  [
    id 101
    name "pantothenate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "pantothenate"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 19.2121875
    size2 4.343625
  ]
  node
  [
    id 102
    name "phenol sulfate"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "phenol sulfate"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 20.381625
    size2 4.343625
  ]
  node
  [
    id 103
    name "propionylcarnitine (C3)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "propionylcarnitine (C3)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 31.741875
    size2 4.343625
  ]
  node
  [
    id 104
    name "prostaglandin F2alpha"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "prostaglandin F2alpha"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 31.2406875
    size2 4.343625
  ]
  node
  [
    id 105
    name "stachydrine"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "stachydrine"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 17.040375
    size2 4.343625
  ]
  node
  [
    id 106
    name "trigonelline (N'-methylnicotinate)"
    group "hypothalamus"
    labelcolor "black"
    labelfamily "sans"
    label "trigonelline (N'-methylnicotinate)"
    color "lightgreen"
    shape "circle"
    labelcex 0.535007826856959
    size 44.1045
    size2 4.343625
  ]
  edge
  [
    source 26
    target 0
    weight -0.634753083404553
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 30
    target 0
    weight 0.813757786392896
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 32
    target 0
    weight 0.874623616230575
    labelcolor "black"
    color "#DE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 33
    target 0
    weight 0.829670572990668
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 35
    target 0
    weight -0.784624241712224
    labelcolor "black"
    color "#009D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 36
    target 0
    weight 0.800017472758327
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 37
    target 0
    weight -0.807512741627213
    labelcolor "black"
    color "#00A800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 38
    target 0
    weight 0.886419397600337
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 42
    target 0
    weight 0.852273297647316
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 43
    target 0
    weight 0.852288719779272
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 44
    target 0
    weight 0.740904226731889
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 45
    target 0
    weight 0.771288987578067
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 46
    target 0
    weight 0.777903354120482
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 49
    target 0
    weight 0.857933313528106
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 50
    target 0
    weight 0.887551678729491
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 53
    target 0
    weight 0.795503829846275
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 54
    target 0
    weight 0.868821043619291
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 0
    weight 0.836357078771172
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 57
    target 0
    weight -0.881674327345821
    labelcolor "black"
    color "#00D300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 0
    weight 0.852208359768207
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 0
    weight 0.814672180941578
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 66
    target 0
    weight -0.81702710804466
    labelcolor "black"
    color "#00AD00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 0
    weight -0.841573899107073
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 0
    weight -0.840317076232684
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 0
    weight 0.850505611402787
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 70
    target 0
    weight 0.817935417484361
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 71
    target 0
    weight 0.803145780288217
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 74
    target 0
    weight 0.788575146667932
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 75
    target 0
    weight 0.772963311468625
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 77
    target 0
    weight 0.824896031223841
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 0
    weight 0.829795568900281
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 56
    target 1
    weight -0.591990004863851
    labelcolor "black"
    color "#004000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 62
    target 1
    weight -0.750507977434804
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 63
    target 1
    weight -0.781559969992579
    labelcolor "black"
    color "#009700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 72
    target 1
    weight -0.755188435968977
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 73
    target 1
    weight -0.76339633782577
    labelcolor "black"
    color "#008D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 26
    target 2
    weight -0.571771402207101
    labelcolor "black"
    color "#003600FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 30
    target 2
    weight 0.715346020618885
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 32
    target 2
    weight 0.773090904463067
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 33
    target 2
    weight 0.701335388051847
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 35
    target 2
    weight -0.701878316832981
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 36
    target 2
    weight 0.692199790032127
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 37
    target 2
    weight -0.699302572280373
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 38
    target 2
    weight 0.775811948028252
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 42
    target 2
    weight 0.719048540904857
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 43
    target 2
    weight 0.746955846541793
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 44
    target 2
    weight 0.622030616103478
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 45
    target 2
    weight 0.68411538295217
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 46
    target 2
    weight 0.666413675515348
    labelcolor "black"
    color "#7F0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 49
    target 2
    weight 0.759886878141668
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 50
    target 2
    weight 0.759567728813668
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 53
    target 2
    weight 0.70177381890701
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 54
    target 2
    weight 0.77166361134829
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 2
    weight 0.749688975428423
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 57
    target 2
    weight -0.749318746154265
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 2
    weight 0.763368438146863
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 2
    weight 0.753662771096606
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 66
    target 2
    weight -0.699769901792398
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 2
    weight -0.752482388580893
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 2
    weight -0.740154524903649
    labelcolor "black"
    color "#008300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 2
    weight 0.776031676057704
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 70
    target 2
    weight 0.73557456104213
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 71
    target 2
    weight 0.731089167382564
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 74
    target 2
    weight 0.644994674327441
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 75
    target 2
    weight 0.667651367364198
    labelcolor "black"
    color "#7F0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 77
    target 2
    weight 0.743975420902175
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 2
    weight 0.761602098701445
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 28
    target 3
    weight 0.58292130560003
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 34
    target 3
    weight 0.566226345851096
    labelcolor "black"
    color "#4A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 39
    target 3
    weight 0.595123395522326
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 41
    target 3
    weight -0.579483333053203
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 47
    target 3
    weight -0.513188510794113
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 51
    target 3
    weight -0.618348476372833
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 52
    target 3
    weight 0.623512953261251
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 26
    target 4
    weight -0.618869155033604
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 30
    target 4
    weight 0.828187208542673
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 32
    target 4
    weight 0.882612517430776
    labelcolor "black"
    color "#DE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 33
    target 4
    weight 0.833084885227838
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 35
    target 4
    weight -0.790501847435705
    labelcolor "black"
    color "#009D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 36
    target 4
    weight 0.8085711263498
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 37
    target 4
    weight -0.808413082677759
    labelcolor "black"
    color "#00A800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 38
    target 4
    weight 0.88949188182614
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 42
    target 4
    weight 0.847079145050126
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 43
    target 4
    weight 0.85010772158668
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 44
    target 4
    weight 0.761134414838126
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 45
    target 4
    weight 0.783155887425684
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 46
    target 4
    weight 0.78668594167633
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 49
    target 4
    weight 0.868795376405491
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 50
    target 4
    weight 0.885487089214895
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 53
    target 4
    weight 0.820073105584281
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 54
    target 4
    weight 0.873250391916338
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 4
    weight 0.842133805836218
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 57
    target 4
    weight -0.880276975423295
    labelcolor "black"
    color "#00D300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 4
    weight 0.86085804247159
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 4
    weight 0.811026996854322
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 66
    target 4
    weight -0.818193008269401
    labelcolor "black"
    color "#00AD00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 4
    weight -0.847078798541061
    labelcolor "black"
    color "#00C300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 4
    weight -0.845007424431673
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 4
    weight 0.854425142314155
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 70
    target 4
    weight 0.835292342335002
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 71
    target 4
    weight 0.796960784371543
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 74
    target 4
    weight 0.798289932080156
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 75
    target 4
    weight 0.781236310352725
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 77
    target 4
    weight 0.819500943968052
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 4
    weight 0.834874382963474
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 41
    target 5
    weight 0.515863264482292
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 58
    target 5
    weight 0.539285383638758
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 59
    target 5
    weight 0.544610688835198
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 41
    target 6
    weight 0.501818449572938
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 58
    target 6
    weight 0.549725016025585
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 59
    target 6
    weight 0.539758035504792
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 7
    weight 0.504745137404941
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 7
    weight -0.500987991040825
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 7
    weight 0.506063620726635
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 7
    weight 0.500141500044535
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 48
    target 8
    weight 0.662083588172705
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 61
    target 8
    weight 0.707933590840376
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 76
    target 8
    weight 0.642276509415336
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 48
    target 9
    weight 0.61395998786165
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 61
    target 9
    weight 0.659634799317486
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 76
    target 9
    weight 0.639432822036987
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 56
    target 10
    weight -0.548663148292304
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 62
    target 10
    weight -0.686685230399022
    labelcolor "black"
    color "#006300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 63
    target 10
    weight -0.722590543899062
    labelcolor "black"
    color "#007700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 72
    target 10
    weight -0.71489359070979
    labelcolor "black"
    color "#007200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 73
    target 10
    weight -0.721738122670403
    labelcolor "black"
    color "#007700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 28
    target 11
    weight 0.600657433069712
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 34
    target 11
    weight 0.592664589268277
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 41
    target 11
    weight -0.54643235130724
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 52
    target 11
    weight 0.566182718258168
    labelcolor "black"
    color "#4A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 59
    target 11
    weight -0.507329414258832
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 28
    target 12
    weight 0.559596455513886
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 34
    target 12
    weight 0.546993402802266
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 41
    target 12
    weight -0.554212488150387
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 47
    target 12
    weight -0.509423250187548
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 52
    target 12
    weight 0.543206378897318
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 26
    target 13
    weight -0.629224277585762
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 30
    target 13
    weight 0.832092018778829
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 32
    target 13
    weight 0.886276174859436
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 33
    target 13
    weight 0.857615085219455
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 35
    target 13
    weight -0.791037888168073
    labelcolor "black"
    color "#009D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 36
    target 13
    weight 0.822865239901848
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 37
    target 13
    weight -0.828293115356189
    labelcolor "black"
    color "#00B300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 38
    target 13
    weight 0.902856340756279
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 42
    target 13
    weight 0.871140028380347
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 43
    target 13
    weight 0.861187326433399
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 44
    target 13
    weight 0.778560457534717
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 45
    target 13
    weight 0.777671365808984
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 46
    target 13
    weight 0.79380462569353
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 49
    target 13
    weight 0.871719926325903
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 50
    target 13
    weight 0.907042324757919
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 53
    target 13
    weight 0.823897325899466
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 54
    target 13
    weight 0.877928043567479
    labelcolor "black"
    color "#DE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 13
    weight 0.848837264898925
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 57
    target 13
    weight -0.906205393709619
    labelcolor "black"
    color "#00E300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 13
    weight 0.862729964042464
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 13
    weight 0.804994384266277
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 66
    target 13
    weight -0.839916009316675
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 13
    weight -0.857546593721695
    labelcolor "black"
    color "#00C800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 13
    weight -0.860834884261714
    labelcolor "black"
    color "#00C800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 13
    weight 0.849018893618844
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 70
    target 13
    weight 0.829784087275055
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 71
    target 13
    weight 0.794443194039027
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 74
    target 13
    weight 0.822726990934111
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 75
    target 13
    weight 0.787087279297324
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 77
    target 13
    weight 0.81959638348192
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 13
    weight 0.826414680602143
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 26
    target 14
    weight -0.574608610452938
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 30
    target 14
    weight 0.72654123194737
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 32
    target 14
    weight 0.786762303311939
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 33
    target 14
    weight 0.767675758253106
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 35
    target 14
    weight -0.698257003148358
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 36
    target 14
    weight 0.740733313990825
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 37
    target 14
    weight -0.7519821422831
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 38
    target 14
    weight 0.807329931790393
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 42
    target 14
    weight 0.772123659254706
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 43
    target 14
    weight 0.775822211765734
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 44
    target 14
    weight 0.677184254879562
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 45
    target 14
    weight 0.691280511469809
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 46
    target 14
    weight 0.702565132525424
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 49
    target 14
    weight 0.775789745489112
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 50
    target 14
    weight 0.807933086454289
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 53
    target 14
    weight 0.728422117723805
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 54
    target 14
    weight 0.787850797698122
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 14
    weight 0.769153035295314
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 57
    target 14
    weight -0.815355721505244
    labelcolor "black"
    color "#00AD00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 14
    weight 0.780176776423484
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 14
    weight 0.745755888429186
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 66
    target 14
    weight -0.756854671573322
    labelcolor "black"
    color "#008D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 14
    weight -0.785354684126135
    labelcolor "black"
    color "#009D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 14
    weight -0.783836982067182
    labelcolor "black"
    color "#009D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 14
    weight 0.772848532979707
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 70
    target 14
    weight 0.733144241013558
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 71
    target 14
    weight 0.729610969122873
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 74
    target 14
    weight 0.707489465528571
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 75
    target 14
    weight 0.696957354985282
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 77
    target 14
    weight 0.741935797448847
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 14
    weight 0.752478100462441
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 15
    weight 0.512091503135295
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 15
    weight 0.508266627870987
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 15
    weight 0.530027653911643
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 15
    weight -0.530382288819431
    labelcolor "black"
    color "#002700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 15
    weight -0.503549720461153
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 15
    weight 0.52401259637601
    labelcolor "black"
    color "#320000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 15
    weight 0.518572465347324
    labelcolor "black"
    color "#320000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 26
    target 16
    weight -0.558241175803108
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 30
    target 16
    weight 0.748976910809481
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 32
    target 16
    weight 0.814480210798192
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 33
    target 16
    weight 0.78141008858438
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 35
    target 16
    weight -0.714481737033749
    labelcolor "black"
    color "#007200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 36
    target 16
    weight 0.764858129348377
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 37
    target 16
    weight -0.760316919578284
    labelcolor "black"
    color "#008D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 38
    target 16
    weight 0.819951612174154
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 42
    target 16
    weight 0.780953108149648
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 43
    target 16
    weight 0.794920532587606
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 44
    target 16
    weight 0.700161935353003
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 45
    target 16
    weight 0.742804807013296
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 46
    target 16
    weight 0.747170630433598
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 49
    target 16
    weight 0.805843829488653
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 50
    target 16
    weight 0.816513188284824
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 53
    target 16
    weight 0.759394761331264
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 54
    target 16
    weight 0.812483084897027
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 16
    weight 0.78259322272606
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 57
    target 16
    weight -0.82558280044621
    labelcolor "black"
    color "#00B300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 16
    weight 0.810276433336017
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 16
    weight 0.77238803424139
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 66
    target 16
    weight -0.767337237310509
    labelcolor "black"
    color "#009200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 16
    weight -0.797581165495468
    labelcolor "black"
    color "#00A300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 16
    weight -0.792176594536755
    labelcolor "black"
    color "#00A300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 16
    weight 0.806015507489882
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 70
    target 16
    weight 0.768955154629741
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 71
    target 16
    weight 0.756707484344008
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 74
    target 16
    weight 0.727332970368965
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 75
    target 16
    weight 0.736650937228707
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 77
    target 16
    weight 0.762729310086916
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 16
    weight 0.788654093871835
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 26
    target 17
    weight -0.621189424418251
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 30
    target 17
    weight 0.83664630614129
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 32
    target 17
    weight 0.897995826798684
    labelcolor "black"
    color "#E60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 33
    target 17
    weight 0.824701677286087
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 35
    target 17
    weight -0.804190189033997
    labelcolor "black"
    color "#00A800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 36
    target 17
    weight 0.815081195361396
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 37
    target 17
    weight -0.808649179011341
    labelcolor "black"
    color "#00A800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 38
    target 17
    weight 0.894190519562612
    labelcolor "black"
    color "#E60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 42
    target 17
    weight 0.834392136668086
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 43
    target 17
    weight 0.859651164685576
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 44
    target 17
    weight 0.75305784796168
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 45
    target 17
    weight 0.811321022621113
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 46
    target 17
    weight 0.79803926929864
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 49
    target 17
    weight 0.886300453591798
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 50
    target 17
    weight 0.878374635746494
    labelcolor "black"
    color "#DE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 53
    target 17
    weight 0.834959372394589
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 54
    target 17
    weight 0.890503183226688
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 17
    weight 0.858466706073438
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 57
    target 17
    weight -0.872255830561405
    labelcolor "black"
    color "#00CE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 17
    weight 0.884955946937711
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 17
    weight 0.84930007138498
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 66
    target 17
    weight -0.814313298565789
    labelcolor "black"
    color "#00AD00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 17
    weight -0.863005002812313
    labelcolor "black"
    color "#00C800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 17
    weight -0.852177420063289
    labelcolor "black"
    color "#00C300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 17
    weight 0.888814836839546
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 70
    target 17
    weight 0.860656548108096
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 71
    target 17
    weight 0.827699586909202
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 74
    target 17
    weight 0.777315547546881
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 75
    target 17
    weight 0.793349589865515
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 77
    target 17
    weight 0.842216631640021
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 17
    weight 0.872767353219203
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 56
    target 18
    weight -0.502652641171887
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 62
    target 18
    weight -0.620317602611161
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 63
    target 18
    weight -0.694494963876124
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 72
    target 18
    weight -0.677722568485293
    labelcolor "black"
    color "#005F00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 73
    target 18
    weight -0.696022998124203
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 56
    target 20
    weight -0.553331115159857
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 62
    target 20
    weight -0.709434898720702
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 63
    target 20
    weight -0.779136095285732
    labelcolor "black"
    color "#009700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 72
    target 20
    weight -0.766479441764585
    labelcolor "black"
    color "#009200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 73
    target 20
    weight -0.78253552382467
    labelcolor "black"
    color "#009700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 48
    target 21
    weight 0.70979608758295
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 61
    target 21
    weight 0.753416230597568
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 76
    target 21
    weight 0.711010920445617
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 56
    target 22
    weight -0.667424667067445
    labelcolor "black"
    color "#005B00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 62
    target 22
    weight -0.838028053036605
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 63
    target 22
    weight -0.85676070460849
    labelcolor "black"
    color "#00C800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 72
    target 22
    weight -0.834957893775535
    labelcolor "black"
    color "#00B800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 73
    target 22
    weight -0.840417121651653
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 30
    target 23
    weight 0.615665922592074
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 32
    target 23
    weight 0.678811070838458
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 33
    target 23
    weight 0.635164790603588
    labelcolor "black"
    color "#6C0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 35
    target 23
    weight -0.596840456447386
    labelcolor "black"
    color "#004000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 36
    target 23
    weight 0.639501526135635
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 37
    target 23
    weight -0.638126730546826
    labelcolor "black"
    color "#005100FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 38
    target 23
    weight 0.682119731572499
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 42
    target 23
    weight 0.61885233421255
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 43
    target 23
    weight 0.662527282383781
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 44
    target 23
    weight 0.559547876919904
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 45
    target 23
    weight 0.622115023582982
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 46
    target 23
    weight 0.605704291067705
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 49
    target 23
    weight 0.676798861030349
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 50
    target 23
    weight 0.661101026819584
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 53
    target 23
    weight 0.642913874868899
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 54
    target 23
    weight 0.685619705009254
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 55
    target 23
    weight 0.675045799034117
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 57
    target 23
    weight -0.674109142649902
    labelcolor "black"
    color "#005F00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 60
    target 23
    weight 0.696778033436123
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 64
    target 23
    weight 0.687703773740947
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 66
    target 23
    weight -0.633198293320095
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 67
    target 23
    weight -0.69492269855073
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 68
    target 23
    weight -0.678462728847573
    labelcolor "black"
    color "#005F00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 69
    target 23
    weight 0.702915602770112
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 70
    target 23
    weight 0.652558875123457
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 71
    target 23
    weight 0.657539915586401
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 74
    target 23
    weight 0.552031018101066
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 75
    target 23
    weight 0.601143894479301
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 77
    target 23
    weight 0.649659743356035
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 78
    target 23
    weight 0.691305615310979
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 40
    target 24
    weight -0.547920481006049
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 65
    target 24
    weight 0.838653316521174
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 48
    target 25
    weight 0.518800776559623
    labelcolor "black"
    color "#320000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 61
    target 25
    weight 0.561808132023992
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 0
    weight 0.785077066336557
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 0
    weight 0.850399084189444
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 0
    weight 0.675812736446549
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 0
    weight 0.784916339442162
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 0
    weight 0.898583149455618
    labelcolor "black"
    color "#E60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 1
    weight -0.696883701877612
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 1
    weight -0.637630570470367
    labelcolor "black"
    color "#005100FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 1
    weight -0.608899905592239
    labelcolor "black"
    color "#004200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 1
    weight 0.514512806441072
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 101
    target 1
    weight -0.528555467660814
    labelcolor "black"
    color "#002700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 2
    weight 0.663170552741418
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 2
    weight 0.773803385854751
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 2
    weight 0.615976396701377
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 2
    weight 0.653267516692116
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 2
    weight 0.786849651925994
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 91
    target 3
    weight -0.521099962950435
    labelcolor "black"
    color "#002300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 3
    weight 0.565931196096762
    labelcolor "black"
    color "#4A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 3
    weight -0.632868017980649
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 4
    weight 0.806984807066811
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 4
    weight 0.859671341323263
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 4
    weight 0.673650316722231
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 4
    weight 0.776593315861918
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 4
    weight 0.903590693756843
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 5
    weight -0.517509106772565
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 6
    weight -0.512483959855832
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 7
    weight 0.51115539642058
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 98
    target 8
    weight -0.656808225268856
    labelcolor "black"
    color "#005700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 98
    target 9
    weight -0.581976653604059
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 10
    weight -0.622670786669175
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 10
    weight -0.576387112778057
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 10
    weight -0.558462897527843
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 10
    weight 0.524790183245521
    labelcolor "black"
    color "#320000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 91
    target 11
    weight -0.594370830075253
    labelcolor "black"
    color "#004000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 11
    weight 0.573816590561808
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 11
    weight -0.561326773390755
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 91
    target 12
    weight -0.541835502827753
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 12
    weight 0.542434791837006
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 12
    weight -0.577658044300852
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 13
    weight 0.814438507957442
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 13
    weight 0.855404460017026
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 13
    weight 0.675486275376664
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 13
    weight 0.814006775274094
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 13
    weight 0.913418374375665
    labelcolor "black"
    color "#EE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 14
    weight 0.701594043674488
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 14
    weight 0.780053578428106
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 14
    weight 0.612918026435555
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 14
    weight 0.729161181986417
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 14
    weight 0.818577210895826
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 15
    weight 0.535403330037587
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 16
    weight 0.748728990038054
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 16
    weight 0.816812277216454
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 16
    weight 0.61658305795375
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 16
    weight 0.708296649424264
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 16
    weight 0.845117099647135
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 17
    weight 0.810990862874327
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 17
    weight 0.894621431882189
    labelcolor "black"
    color "#E60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 17
    weight 0.689372453870219
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 17
    weight 0.746832553170398
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 17
    weight 0.915522016190643
    labelcolor "black"
    color "#EE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 18
    weight -0.634116380408841
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 18
    weight -0.567849394952345
    labelcolor "black"
    color "#003600FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 18
    weight -0.6185074281635
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 19
    weight -0.507145564921879
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 20
    weight -0.706201477136187
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 20
    weight -0.630198159843615
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 20
    weight -0.663571576506999
    labelcolor "black"
    color "#005700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 20
    weight 0.517789595896597
    labelcolor "black"
    color "#2D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 98
    target 21
    weight -0.686797489425198
    labelcolor "black"
    color "#006300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 22
    weight -0.754092712559997
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 22
    weight -0.696076421647891
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 22
    weight -0.650884116007408
    labelcolor "black"
    color "#005400FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 22
    weight 0.582668767889797
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 101
    target 22
    weight -0.573561980053956
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 23
    weight 0.591897436342357
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 23
    weight 0.716180288029138
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 23
    weight 0.534698830884291
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 23
    weight 0.563643064969173
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 23
    weight 0.704974968255588
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 80
    target 24
    weight 0.612828097423692
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 89
    target 24
    weight 0.554575285620333
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 90
    target 24
    weight 0.60872953448563
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 97
    target 24
    weight -0.714234666839445
    labelcolor "black"
    color "#007200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 100
    target 24
    weight 0.527925205103178
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 103
    target 24
    weight -0.537717093910135
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 104
    target 24
    weight -0.646263498818071
    labelcolor "black"
    color "#005400FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 98
    target 25
    weight -0.531032267108518
    labelcolor "black"
    color "#002700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 26
    weight -0.63310350696749
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 26
    weight -0.608042101609507
    labelcolor "black"
    color "#004200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 26
    weight -0.613819716735595
    labelcolor "black"
    color "#004700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 26
    weight -0.667900356084467
    labelcolor "black"
    color "#005B00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 27
    weight 0.563253766905697
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 83
    target 28
    weight -0.50901301708689
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 91
    target 28
    weight -0.733057592481538
    labelcolor "black"
    color "#007D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 28
    weight 0.684825772919403
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 28
    weight -0.655568595926666
    labelcolor "black"
    color "#005700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 99
    target 28
    weight -0.536900926391768
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 29
    weight 0.50480528493482
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 30
    weight 0.786017120269131
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 30
    weight 0.845783532789169
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 30
    weight 0.692534888679109
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 30
    weight 0.737180512187636
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 30
    weight 0.884818793927636
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 31
    weight -0.570511189917167
    labelcolor "black"
    color "#003600FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 31
    weight 0.53693136991648
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 32
    weight 0.840995404211631
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 32
    weight 0.925911959574532
    labelcolor "black"
    color "#F20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 32
    weight 0.738631991112713
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 32
    weight 0.770196656541343
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 32
    weight 0.956142358133441
    labelcolor "black"
    color "#FF0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 33
    weight 0.796628345197894
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 33
    weight 0.837865787226151
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 33
    weight 0.62750249849659
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 33
    weight 0.797735392281848
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 33
    weight 0.885316265193288
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 83
    target 34
    weight -0.51077283870189
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 91
    target 34
    weight -0.724455867264214
    labelcolor "black"
    color "#007700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 34
    weight 0.676257702258122
    labelcolor "black"
    color "#840000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 34
    weight -0.64008475809547
    labelcolor "black"
    color "#005100FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 99
    target 34
    weight -0.52569944941013
    labelcolor "black"
    color "#002300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 35
    weight -0.723556469257742
    labelcolor "black"
    color "#007700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 35
    weight -0.821205683057731
    labelcolor "black"
    color "#00B300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 35
    weight -0.703297243095656
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 35
    weight -0.699762571943472
    labelcolor "black"
    color "#006800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 35
    weight -0.85329902448557
    labelcolor "black"
    color "#00C300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 36
    weight 0.787819654834794
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 36
    weight 0.839080862835242
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 36
    weight 0.611604278904268
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 36
    weight 0.743342292120951
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 36
    weight 0.868842951006154
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 37
    weight -0.748792464224056
    labelcolor "black"
    color "#008800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 37
    weight -0.503071365346331
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 37
    weight -0.826198882996389
    labelcolor "black"
    color "#00B300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 37
    weight -0.63615787666226
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 37
    weight -0.783604809587323
    labelcolor "black"
    color "#009D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 37
    weight -0.864093045507358
    labelcolor "black"
    color "#00C800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 38
    weight 0.819745177695089
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 38
    weight 0.911641091224015
    labelcolor "black"
    color "#EE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 38
    weight 0.740908795238741
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 38
    weight 0.829132236706441
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 38
    weight 0.953640403688107
    labelcolor "black"
    color "#FF0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 79
    target 39
    weight -0.509185403627726
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 39
    weight 0.621045313903112
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 39
    weight -0.701022957590919
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 79
    target 41
    weight 0.587555559438647
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 83
    target 41
    weight 0.562421246952002
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 91
    target 41
    weight 0.55728750342418
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 41
    weight -0.700844727619347
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 41
    weight 0.764277765506621
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 42
    weight 0.789092748048884
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 42
    weight 0.847899621344078
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 42
    weight 0.686571189535004
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 42
    weight 0.795430038587403
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 42
    weight 0.903852036697345
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 43
    weight 0.795725980900021
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 43
    weight 0.892042016627465
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 43
    weight 0.707961945271385
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 43
    weight 0.764662279829708
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 43
    weight 0.923300856830116
    labelcolor "black"
    color "#F20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 44
    weight 0.760691089083403
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 44
    weight 0.749496423974282
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 44
    weight 0.545912039420394
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 44
    weight 0.712404275820444
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 44
    weight 0.797174173782548
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 45
    weight 0.807308939114303
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 45
    weight 0.861408023424862
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 45
    weight 0.620075997716635
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 45
    weight 0.608441062917121
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 45
    weight 0.866714926120366
    labelcolor "black"
    color "#D90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 46
    weight 0.813770193326502
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 46
    weight 0.837799013527301
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 46
    weight 0.589279284136466
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 46
    weight 0.644773778734788
    labelcolor "black"
    color "#710000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 46
    weight 0.859272914487701
    labelcolor "black"
    color "#D60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 79
    target 47
    weight 0.545998892765373
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 83
    target 47
    weight 0.518463680012495
    labelcolor "black"
    color "#320000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 47
    weight -0.632687808829821
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 47
    weight 0.697693054407657
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 84
    target 48
    weight -0.504033419952449
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 85
    target 48
    weight -0.608548718943274
    labelcolor "black"
    color "#004200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 98
    target 48
    weight -0.89619161047102
    labelcolor "black"
    color "#00DE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 49
    weight 0.835995003939665
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 49
    weight 0.914734717530406
    labelcolor "black"
    color "#EE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 49
    weight 0.713510281865907
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 49
    weight 0.758372596865506
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 49
    weight 0.941031118711761
    labelcolor "black"
    color "#FA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 50
    weight 0.81789219295874
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 50
    weight 0.892047483263139
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 50
    weight 0.723628523356825
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 50
    weight 0.83748648786333
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 50
    weight 0.944957643812613
    labelcolor "black"
    color "#FA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 91
    target 52
    weight -0.707422523340856
    labelcolor "black"
    color "#006D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 52
    weight 0.667422072013742
    labelcolor "black"
    color "#7F0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 52
    weight -0.651383395818095
    labelcolor "black"
    color "#005400FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 99
    target 52
    weight -0.555308226726741
    labelcolor "black"
    color "#003200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 53
    weight 0.808443439710296
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 53
    weight 0.846858300443606
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 53
    weight 0.62725348009988
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 53
    weight 0.739502519955925
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 53
    weight 0.874184215428379
    labelcolor "black"
    color "#DE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 54
    weight 0.823116897067781
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 54
    weight 0.922985144963434
    labelcolor "black"
    color "#F20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 54
    weight 0.733165959091661
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 54
    weight 0.771451442429223
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 54
    weight 0.949159109187534
    labelcolor "black"
    color "#FF0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 55
    weight 0.769524439425181
    labelcolor "black"
    color "#AE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 55
    weight 0.881298809481418
    labelcolor "black"
    color "#DE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 55
    weight 0.709040816890407
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 55
    weight 0.785426573685144
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 55
    weight 0.908123325756061
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 56
    weight 0.536557480679384
    labelcolor "black"
    color "#3C0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 56
    weight 0.575083248210879
    labelcolor "black"
    color "#4F0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 56
    weight -0.535759468141418
    labelcolor "black"
    color "#002700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 57
    weight -0.826070426193782
    labelcolor "black"
    color "#00B300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 57
    weight -0.507200708563871
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 57
    weight -0.889286847203206
    labelcolor "black"
    color "#00D800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 57
    weight -0.682826212186198
    labelcolor "black"
    color "#006300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 57
    weight -0.846488802413231
    labelcolor "black"
    color "#00BE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 57
    weight -0.938758529314763
    labelcolor "black"
    color "#00F800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 83
    target 58
    weight 0.560411363065523
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 58
    weight -0.575022067169
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 58
    weight 0.527517573818132
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 79
    target 59
    weight 0.534316189522982
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 83
    target 59
    weight 0.601845233367719
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 91
    target 59
    weight 0.530361288187857
    labelcolor "black"
    color "#370000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 92
    target 59
    weight -0.635319099972044
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 93
    target 59
    weight 0.617819035813242
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 60
    weight 0.824511359862809
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 60
    weight 0.922531371897474
    labelcolor "black"
    color "#F20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 60
    weight 0.704448639264183
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 60
    weight 0.755520690765022
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 60
    weight 0.938749277839673
    labelcolor "black"
    color "#FA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 84
    target 61
    weight -0.510049926002115
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 85
    target 61
    weight -0.619072007746902
    labelcolor "black"
    color "#004A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 98
    target 61
    weight -0.91807492358511
    labelcolor "black"
    color "#00E900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 62
    weight 0.756391747580634
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 62
    weight 0.718102421318247
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 62
    weight 0.584233069251916
    labelcolor "black"
    color "#540000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 62
    weight -0.647322752176702
    labelcolor "black"
    color "#005400FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 101
    target 62
    weight 0.616904791499213
    labelcolor "black"
    color "#630000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 63
    weight 0.8181983947978
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 63
    weight 0.756239220991523
    labelcolor "black"
    color "#A90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 63
    weight 0.712199692599981
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 63
    weight -0.634069035225104
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 95
    target 63
    weight -0.507328973904878
    labelcolor "black"
    color "#001200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 101
    target 63
    weight 0.621395061773492
    labelcolor "black"
    color "#670000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 64
    weight 0.747126922475839
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 64
    weight 0.907491191664891
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 64
    weight 0.720948968356232
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 64
    weight 0.690417276872929
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 64
    weight 0.905021366029989
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 80
    target 65
    weight 0.71307674206144
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 82
    target 65
    weight 0.50506744190957
    labelcolor "black"
    color "#1A0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 89
    target 65
    weight 0.725671157123069
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 90
    target 65
    weight 0.713560996673421
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 97
    target 65
    weight -0.789801949376375
    labelcolor "black"
    color "#009D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 100
    target 65
    weight 0.657242337878974
    labelcolor "black"
    color "#7B0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 103
    target 65
    weight -0.569494965366902
    labelcolor "black"
    color "#003600FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 104
    target 65
    weight -0.575196895554401
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 66
    weight -0.767435960695141
    labelcolor "black"
    color "#009200FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 66
    weight -0.83016092891844
    labelcolor "black"
    color "#00B800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 66
    weight -0.636121244640544
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 66
    weight -0.786227058539574
    labelcolor "black"
    color "#009D00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 66
    weight -0.872835737951263
    labelcolor "black"
    color "#00CE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 67
    weight -0.776551711486222
    labelcolor "black"
    color "#009700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 67
    weight -0.524256016014905
    labelcolor "black"
    color "#002300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 67
    weight -0.887911572890506
    labelcolor "black"
    color "#00D800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 67
    weight -0.690103602562046
    labelcolor "black"
    color "#006300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 67
    weight -0.808099832251596
    labelcolor "black"
    color "#00A800FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 67
    weight -0.912541056651435
    labelcolor "black"
    color "#00E900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 68
    weight -0.774300936761648
    labelcolor "black"
    color "#009700FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 68
    weight -0.538918243122586
    labelcolor "black"
    color "#002A00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 68
    weight -0.869573542058806
    labelcolor "black"
    color "#00CE00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 68
    weight -0.677693972860152
    labelcolor "black"
    color "#005F00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 68
    weight -0.820823192725147
    labelcolor "black"
    color "#00B300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 68
    weight -0.903634854883252
    labelcolor "black"
    color "#00E300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 69
    weight 0.809798755308532
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 69
    weight 0.939621201113677
    labelcolor "black"
    color "#FA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 69
    weight 0.735122969491089
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 69
    weight 0.720885708358256
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 69
    weight 0.944768701471052
    labelcolor "black"
    color "#FA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 70
    weight 0.812046193726988
    labelcolor "black"
    color "#C10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 70
    weight 0.888302356809732
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 70
    weight 0.696966327856286
    labelcolor "black"
    color "#8D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 70
    weight 0.705431833842049
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 70
    weight 0.906956094499182
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 71
    weight 0.742306827773615
    labelcolor "black"
    color "#A20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 71
    weight 0.883717966924649
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 71
    weight 0.702970983026021
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 71
    weight 0.670975770024018
    labelcolor "black"
    color "#7F0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 71
    weight 0.88816991872201
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 72
    weight 0.80999320146086
    labelcolor "black"
    color "#BE0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 72
    weight 0.720673633490192
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 72
    weight 0.717154415689434
    labelcolor "black"
    color "#950000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 72
    weight -0.636270995901526
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 101
    target 72
    weight 0.554579796852767
    labelcolor "black"
    color "#400000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 81
    target 73
    weight 0.825364742351317
    labelcolor "black"
    color "#C60000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 83
    target 73
    weight -0.510454778337386
    labelcolor "black"
    color "#002000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 86
    target 73
    weight 0.729729703628208
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 88
    target 73
    weight 0.7486040546074
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 94
    target 73
    weight -0.631277548156365
    labelcolor "black"
    color "#004E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 101
    target 73
    weight 0.555662434930948
    labelcolor "black"
    color "#450000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 74
    weight 0.791180728789308
    labelcolor "black"
    color "#B50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 74
    weight 0.774230186323909
    labelcolor "black"
    color "#B10000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 74
    weight 0.592568510174725
    labelcolor "black"
    color "#580000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 74
    weight 0.736270695094485
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 74
    weight 0.837700294462387
    labelcolor "black"
    color "#CD0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 75
    weight 0.792627676559814
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 75
    weight 0.829515121130576
    labelcolor "black"
    color "#C90000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 75
    weight 0.603122703164604
    labelcolor "black"
    color "#5D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 75
    weight 0.649013613236084
    labelcolor "black"
    color "#750000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 75
    weight 0.85194879357484
    labelcolor "black"
    color "#D20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 84
    target 76
    weight -0.550860368982514
    labelcolor "black"
    color "#002E00FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 85
    target 76
    weight -0.581295706897394
    labelcolor "black"
    color "#003900FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 98
    target 76
    weight -0.824357698901254
    labelcolor "black"
    color "#00B300FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 77
    weight 0.750633438745267
    labelcolor "black"
    color "#A50000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 77
    weight 0.885257254056151
    labelcolor "black"
    color "#E20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 77
    weight 0.73476454724183
    labelcolor "black"
    color "#9D0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 77
    weight 0.705548022317722
    labelcolor "black"
    color "#910000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 77
    weight 0.903248002925532
    labelcolor "black"
    color "#EA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 87
    target 78
    weight 0.79641123423801
    labelcolor "black"
    color "#BA0000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 96
    target 78
    weight 0.926173911814186
    labelcolor "black"
    color "#F20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 102
    target 78
    weight 0.721422403102818
    labelcolor "black"
    color "#990000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 105
    target 78
    weight 0.691323554642791
    labelcolor "black"
    color "#890000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
  edge
  [
    source 106
    target 78
    weight 0.926585874910854
    labelcolor "black"
    color "#F20000FF"
    lty "solid"
    width 1
    labelcex 0.481507044171263
  ]
]
